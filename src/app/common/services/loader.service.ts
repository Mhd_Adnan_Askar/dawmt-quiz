import { Injectable } from '@angular/core';
import { BehaviorSubject, map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  loading$= new BehaviorSubject<string[]>([])
  showLoading$ = this.loading$.pipe(
    map(notFinishRequests=>!!notFinishRequests.length)
  )
  show(){
    let state = [...this.loading$.value]
    state.push(''+state.length)
    this.loading$.next(state)
  }
  hide(){
    let state = [...this.loading$.value]
    state.pop()
    this.loading$.next(state)
  }

  constructor() { }
}
