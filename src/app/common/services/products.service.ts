import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Product } from '../../../models/product';

const { apiUrl } = environment;
@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(
    private http: HttpClient
  ) { }
  getAllproducts(){
    return this.http.get<Product[]>(`${apiUrl}/products`)
  }
  getAllproductsById(id:string){
    return this.http.get<Product>(`${apiUrl}/products/${id}`)
  }
}
