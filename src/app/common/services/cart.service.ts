import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Product } from '../../../models/product';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  cart$ = new BehaviorSubject<Product[]>([])
  constructor() { }

  addToCart(product: Product) {
    const currentCart = this.cart$.value
    this.cart$.next([product, ...currentCart])
  }
  removeFromCart(product: Product) {
    const currentCart = this.cart$.value
    const patchedCart = currentCart.filter(item => item.id !== product.id)
    this.cart$.next(patchedCart)
  }
}
