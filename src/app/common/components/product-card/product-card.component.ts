import { Component, Input, inject } from '@angular/core';
import { Product } from '../../../../models/product';
import { AsyncPipe, NgIf, NgStyle, UpperCasePipe } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { CartService } from '../../services/cart.service';
import { map } from 'rxjs';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-product-card',
  standalone: true,
  imports: [UpperCasePipe, NgStyle, MatIconModule, MatButtonModule, NgIf, AsyncPipe, RouterLink],
  templateUrl: './product-card.component.html',
  styleUrl: './product-card.component.scss'
})
export class ProductCardComponent {
  @Input() product !: Product
  cartService = inject(CartService)
  isProductInCart$ = this.cartService.cart$.pipe(
    map(items => items.some(item => item.id === this.product.id))
  )

  addItemToCart() {
    this.cartService.addToCart(this.product)
  }
  removeFromCart(){
    this.cartService.removeFromCart(this.product)
  }
}
