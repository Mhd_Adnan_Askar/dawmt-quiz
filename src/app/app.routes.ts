import { Routes } from '@angular/router';
import {routes as mainRoutes} from './main-layout/main.routes'

export const routes: Routes = [
  {
    path:'',
    loadComponent: ()=>import('./main-layout/main-layout.component').then(c=>c.MainLayoutComponent),
    children: mainRoutes
  },
];
