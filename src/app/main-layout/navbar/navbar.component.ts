import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, ViewChild, inject } from '@angular/core';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { RouterLink, RouterModule } from '@angular/router';
import { CartService } from '../../common/services/cart.service';
import { Subscription, combineLatest, map, share, startWith } from 'rxjs';
import { AsyncPipe, NgFor, NgIf, NgStyle } from '@angular/common';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { ProductsService } from '../../common/services/products.service';
import {MatListModule} from '@angular/material/list';

@Component({
  selector: 'app-navbar',
  standalone: true,
  imports: [MatToolbarModule, MatIconModule, MatButtonModule, RouterLink, RouterModule, AsyncPipe, NgIf, MatFormFieldModule, MatInputModule, ReactiveFormsModule, NgFor, NgStyle,MatListModule],
  templateUrl: './navbar.component.html',
  styleUrl: './navbar.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavbarComponent implements OnDestroy {
  productsService = inject(ProductsService)
  cartService = inject(CartService)
  cartItemsQuantity$ = this.cartService.cart$.pipe(
    map(items => items.length)
  )
  products$ = this.productsService.getAllproducts().pipe(share())
  searchFormControl = new FormControl('')
  filteredProducts$ = combineLatest([
    this.products$,
    this.searchFormControl.valueChanges.pipe(startWith(''))
  ]).pipe(
    map(([products, search]) => {
      if (!search) {
        return [];
      }
      return products.filter(p => p.title.includes(search)|| p.category.includes(search));
    }),
  );

  subscriptions$ = new Subscription()
  cdr = inject(ChangeDetectorRef);
  constructor() {
    this.subscriptions$.add(
      this.filteredProducts$.subscribe(() => {
        this.cdr.detectChanges();
      })
    )
  }
  ngOnDestroy(): void {
      this.subscriptions$.unsubscribe()
  }

  clearSearch(){
    this.searchFormControl.patchValue('')
  }
}
