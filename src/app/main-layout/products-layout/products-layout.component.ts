import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, inject } from '@angular/core';
import { ProductsService } from '../../common/services/products.service';
import { ProductCardComponent } from '../../common/components/product-card/product-card.component';
import { AsyncPipe, NgFor } from '@angular/common';
import { Subscription, share } from 'rxjs';

@Component({
  selector: 'app-products-layout',
  standalone: true,
  imports: [ProductCardComponent, AsyncPipe, NgFor],
  templateUrl: './products-layout.component.html',
  styleUrl: './products-layout.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductsLayoutComponent implements OnDestroy {
  productsService = inject(ProductsService)
  products$ = this.productsService.getAllproducts().pipe(share())

  subscriptions$ = new Subscription()
  cdr = inject(ChangeDetectorRef);
  constructor() {
    this.subscriptions$.add(
      this.products$.subscribe(() => {
        this.cdr.detectChanges();
      })
    )
  }
  ngOnDestroy(): void {
      this.subscriptions$.unsubscribe()
  }
}
