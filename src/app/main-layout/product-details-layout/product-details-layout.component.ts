import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, inject } from '@angular/core';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { Subscription, combineLatest, map, share, switchMap, tap } from 'rxjs';
import { ProductsService } from '../../common/services/products.service';
import { AsyncPipe, NgIf, NgStyle, UpperCasePipe } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { CartService } from '../../common/services/cart.service';
import { Product } from '../../../models/product';

@Component({
  selector: 'app-product-details-layout',
  standalone: true,
  imports: [NgIf, AsyncPipe,NgStyle, UpperCasePipe, MatIconModule, MatButtonModule, RouterLink],
  templateUrl: './product-details-layout.component.html',
  styleUrl: './product-details-layout.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductDetailsLayoutComponent implements OnDestroy{
  productsService = inject(ProductsService)
  route = inject(ActivatedRoute)
  router = inject(Router)
  id$ = this.route.paramMap.pipe(map(r=>r.get('id')))
  product$ = this.id$.pipe(
    switchMap(id=>this.productsService.getAllproductsById(id!)),
    share(),
    tap(prod=>{
      if(!prod){
        this.router.navigate(['/products'])
      }
    })
  )
  cartService = inject(CartService)
  cart$ = this.cartService.cart$
  isProductInCart$= combineLatest([this.product$,this.cart$]).pipe(
    map(([product, cartItems])=>cartItems.some(item => item.id === product.id))
  )


  subscriptions$ = new Subscription()
  cdr = inject(ChangeDetectorRef);
  constructor() {
    this.subscriptions$.add(
      this.product$.subscribe(() => {
        this.cdr.detectChanges();
      })
    )
  }
  ngOnDestroy(): void {
      this.subscriptions$.unsubscribe()
  }

  addItemToCart(product:Product) {
    this.cartService.addToCart(product)
  }
  removeFromCart(product:Product){
    this.cartService.removeFromCart(product)
  }
}
