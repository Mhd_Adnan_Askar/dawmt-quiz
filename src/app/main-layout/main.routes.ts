import { Routes } from '@angular/router';

export const routes: Routes = [
  {
    path: '',
    loadComponent: () => import('./home-layout/home-layout.component').then(c => c.HomeLayoutComponent)
  },
  {
    path: 'products',
    loadComponent: () => import('./products-layout/products-layout.component').then(c => c.ProductsLayoutComponent),
  },
  {
    path: 'products/:id',
    loadComponent: () => import('./product-details-layout/product-details-layout.component').then(c => c.ProductDetailsLayoutComponent),
  }
];
