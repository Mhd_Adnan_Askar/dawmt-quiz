import { Rating } from "./rating";

export interface Product {
  id: number,
  title: string,
  image: string,
  description: string,
  category: string,
  price: number,
  rating: Rating
}
